<?php
/**
 * The header for our theme.
 *
 * @package shop-isle
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="facebook-domain-verification" content="7lizeiq8229xda2doptw83zrr0rmjm" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php } ?>
	<?php wp_head(); ?>
	<!-- Meta Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '216750751159058');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=216750751159058&ev=PageView&noscript=1"/></noscript>
	<!-- End Meta Pixel Code -->
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<?php do_action( 'shop_isle_before_header' ); ?>

	<!-- Preloader -->
	<?php

	/* Preloader */
	if ( is_front_page() && ! is_customize_preview() ) :

		$shop_isle_disable_preloader = get_theme_mod( 'shop_isle_disable_preloader' );

		if ( isset( $shop_isle_disable_preloader ) && ( $shop_isle_disable_preloader != 1 ) ) :

			echo '<div class="page-loader">';
				echo '<div class="loader">' . __( 'Loading...', 'shop-isle' ) . '</div>';
			echo '</div>';

		endif;

	endif;

	$header_class = '';
	$hide_top_bar = get_theme_mod( 'shop_isle_top_bar_hide', true );
	if ( (bool) $hide_top_bar === false ) {
		$header_class .= 'header-with-topbar';
	}
	?>

	<header class="header <?php echo esc_attr( $header_class ); ?>">
	<?php do_action( 'shop_isle_header' ); ?>

	<?php do_action( 'shop_isle_after_header' ); ?>

	</header>
