<?php

/* Template Name: Payment Confirmation */

// Retrieve order
$order_id = base64_decode($_GET['order']);
$order = wc_get_order($order_id);

$uploaded_proof_of_payment = false;

if (!empty($_FILES['semilir_pop'])) {
  // Rename file name to orde rnumber
  $info = pathinfo($_FILES['semilir_pop']['name']);
  $ext = empty($info['extension']) ? '' : '.' . $info['extension'];
  $_FILES['semilir_pop']["name"] = "order_{$order->get_id()}.{$ext}";

  // Upload file to specific folder
  function proof_of_payment_upload_directory($dir)
  {
    return [
      'path' => $dir['basedir'] . '/proof_of_payments',
      'url' => $dir['baseurl'] . '/proof_of_payments',
      'subdir' => '/proof_of_payments',
    ] + $dir;
  }
  add_filter('upload_dir', 'proof_of_payment_upload_directory');
  $upload_overrides = array('test_form' => false);
  $result = wp_handle_upload($_FILES['semilir_pop'], $upload_overrides);
  remove_filter('upload_dir', 'proof_of_payment_upload_directory');

  // E-mail semilir about proof of payment
  $subject = 'Payment confirmation of order #' . $order->get_id();
  $message = "<p>Hello Semilir!</p><p>Customer {$order->get_formatted_billing_full_name()} has uploaded a proof of payment for order <a href='" . site_url() . "/wp-admin/post.php?post={$order->get_id()}&action=edit'>#{$order->get_id()}</a>. You can <a href='{$result['url']}'>view it here</a>.</p>";
  wp_mail('hello@semilirsemilir.com', $subject, $message);

  $uploaded_proof_of_payment = true;
}

get_header(); ?>
<!-- Wrapper start -->
<div class="main">

  <section class="module">
    <div class="container">
      <h1>Payment Confirmation</h1>
      <?php
      if ($uploaded_proof_of_payment === true) {
        echo "<p>Thank you for your payment confirmation, {$order->get_formatted_billing_full_name()}. We will now process your order.</p>";
      }
      elseif (!$order) {
        echo "<p>Sorry, we cannot find this order.</p>";
      } else {
        echo "
        <p>Hello {$order->get_formatted_billing_full_name()},</p>
        <p>Please upload proof of payment for your order below.</p>
        <table style='width: 300px; border: 1px solid black;'>
          <tr>
            <td style='width: 100px; padding:1rem; font-weight: bold;'>Order no.</th>
            <td style='width: 200px; padding:1rem; font-weight: bold;'>Amount</td>
          </tr>
          <tr>
            <td style='padding:0 1rem 1rem;'>{$order->get_order_number()}</td>
            <td style='padding:0 1rem 1rem;'>Rp ".number_format($order->get_total(), 0, '', '.')."</td>
          </tr>
        </table>
        <form method='post' enctype='multipart/form-data' action=''>
          <input type='hidden' name='semilir_order_id' value='{$order->get_id()}' />
          <label for='payment_confirmation_file'>Choose file to upload</label>
          <input id='payment_confirmation_file' name='semilir_pop' type='file' accept='image/*,.pdf' size='5000000' />
          <p style='font-style:italic;'>Only images or .pdf files are allowed. Maximum 5MB.</p>
          <input type='submit' value='Submit proof' class='single_add_to_cart_button' />
        </form>";
      } ?>
    </div>
  </section>

</div>

<?php get_footer(); ?>
