# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Don't forget to also update the version in the `src/package.json` file with each update.

---
[1.1.2] Added proof of payment template page
[1.1.1] Removed plugin wpforms-lite, added plugins fluent-smtp and wp-mail-logging
[1.1.0] Updated PHP (v7.4 => v8.1)
[1.1.2] Updated plugin `mailchimp-for-woocommerce` (2.8.3 => 4.4.1)
[1.1.1] Added composer image to docker compose
[1.1.0] Updated multiple WordPress plugins
  - Upgrading composer/semver (3.4.0 => 3.4.3)
  - Upgrading phpoption/phpoption (1.9.2 => 1.9.3)
  - Upgrading roave/security-advisories (dev-master 2b23329 => dev-master 4e4266a)
  - Upgrading roots/wordpress (6.4.1 => 6.6.2)
  - Upgrading roots/wordpress-no-content (6.4.1 => 6.6.2)
  - Upgrading squizlabs/php_codesniffer (3.7.2 => 3.10.3)
  - Upgrading symfony/deprecation-contracts (v2.5.2 => v2.5.3)
  - Upgrading symfony/finder (v5.4.27 => v5.4.43)
  - Upgrading symfony/polyfill-ctype (v1.28.0 => v1.31.0)
  - Upgrading symfony/polyfill-php80 (v1.28.0 => v1.31.0)
  - Upgrading wp-cli/core-command (v2.1.15 => v2.1.18)
  - Upgrading wp-cli/entity-command (v2.5.6 => v2.8.1)
  - Upgrading wp-cli/php-cli-tools (v0.11.21 => v0.11.22)
  - Upgrading wp-cli/wp-cli (v2.9.0 => v2.11.0)
  - Upgrading wpackagist-plugin/woocommerce (7.9.0 => 9.3.3)
  - Upgrading wpackagist-plugin/wpforms-lite (1.8.4.1 => 1.9.1.3)

[1.0.1] Updated WordPress to v6.2.2
[1.0.0] Add Meta Pixel

[0.1.3] Ignored Snyk (deep code) cache
[0.1.2] Updated plugin Mailchimp for WooCommerce to v2.7.6
[0.1.1] Updated plugin WPForms Lite to v1.7.8
[0.1.0] Updated plugin WooCommerce to v7.10
[0.0.2] Dockerized application
[0.0.1] Initial version of `CHANGELOG.md`
