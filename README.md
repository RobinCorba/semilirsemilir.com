# Website of Semilir

* WordPress-based website developed by [Robin Corba](https://www.robincorba.com)

___

## Update instructions

To update this project you have to manually adjust versions in the following files:

./Dockerfile
./docker-compose.yml
/src/composer.json
/src/app/web/themes/composer.json
/src/app/web/themes/package.json

___

## Installation instructions

### Prerequisites

Make sure you have this software installed on your computer:

| Software        | Version       |
| -------------   | ------------- |
| `docker`        | 20            |
| `docker-compose`| 1.27          |

___

### How to set up the development environment

1. Copy the `.env.example` to `.env` and fill in the details
1. Enable SSL on the local environment;
   * Go to the CLI folder with `cd ./cli` and run the following scripts:
      * Add `www.semilirsemilir.test` to your local known_hosts: `./setup-hosts-file.sh`
1. Create a symbolic link of environment variables with: `cd ../src && ln -s ../.env .`
1. Run `docker compose up -d` from the root folder to start all containers
1. Access the Semilir website container with `docker exec -it semilir-site bash`
    * Install Bedrock (WordPress) dependencies with `cd /var/www/html && composer install`
    * Exit the the container with `exit`
1. Access the website on http://www.semilirsemilir.test. Happy coding! (next time you only have to run `docker compose up -d`)

___

### Notes

1. Access CMS on http://www.semilirsemilir.test/wp-admin
1. Access PHPMyAdmin on http://localhost:8080
